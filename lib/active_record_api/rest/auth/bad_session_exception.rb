module ActiveRecordApi
  module Rest
    module Auth
      class BadSessionException < StandardError
        attr_reader :action
        attr_reader :controller
        def initialize(controller, action)
          super("No user for fullmeasure_session on #{action} #{controller}")
          @action = action
          @controller = controller
        end
      end
    end
  end
end
