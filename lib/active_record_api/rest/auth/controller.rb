module ActiveRecordApi
  module Rest
    module Auth
      class Controller < ActiveRecordApi::Rest::Controller
        before_action :authorize

        protected

        def authorize
          return true if authenticated_internally?
          raise BadSessionException.new(controller_name, action_name) if fullmeasure_session.nil?
          raise AccessDeniedException.new(controller_name, action_name, 'Insufficient permissions') unless can?
        end

        def can?
          @can ||= policy.can?
        end

        def policy
          @policy ||= policy_klass.new(
            fullmeasure_session: fullmeasure_session,
            model_klass: model_klass,
            action_name: action_name,
            params: params,
            queryable_params: queryable_params,
            modifiable_params: modifiable_params
          )
        end

        def policy_klass
          "Policy::#{self.class.name.remove(/Controller$/)}Policy".safe_constantize || "#{self.class.name.remove(/Controller$/)}Policy".safe_constantize || Policy
        end

        # For internal communication between different APIs, we use a common, rotating
        # key as authentication token. This allows us to avoid expensive authenticaion
        # requests and visits to postgres and redis.
        #
        # If the incoming request has this token, it's assumed to be originating from
        # FME APIs and can be marked authenticated.
        def authenticated_internally?
          header = request.headers['Authorization'] || request.headers['authorization']
          token = header&.split(' ')&.last
          safe_word = ENV['FME_INTERNAL_API_TOKEN']
          # Compare the tokens in a time-constant manner, to mitigate
          # timing attacks.
          token.present? &&
            safe_word.present? &&
            ActiveSupport::SecurityUtils.secure_compare(token, safe_word)
        end
      end
    end
  end
end
