module ActiveRecordApi
  module Rest
    module Auth
      class Policy
        include ActiveAttr::Model
        attr_accessor :fullmeasure_session, :model_klass, :action_name, :modifiable_params, :queryable_params, :params

        def can?
          send("can_#{action_name}?")
        end

        # :nocov:
        def can_manage?
          false
        end

        def can_read?
          false
        end
        # :nocov:

        protected

        def can_index?
          can_read? || can_manage?
        end

        def can_show?
          can_read? || can_manage?
        end

        # :nocov:
        def can_new?
          can_read? || can_manage?
        end
        # :nocov:

        def can_create?
          can_manage?
        end

        def can_update?
          can_manage?
        end

        def can_destroy?
          can_manage?
        end
      end
    end
  end
end
