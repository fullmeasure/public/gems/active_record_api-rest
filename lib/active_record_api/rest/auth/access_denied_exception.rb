module ActiveRecordApi
  module Rest
    module Auth
      class AccessDeniedException < StandardError
        attr_reader :action
        attr_reader :controller
        def initialize(controller, action, message)
          super(message)
          @action = action
          @controller = controller
        end
      end
    end
  end
end
