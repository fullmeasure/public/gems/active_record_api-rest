require 'active_attr'

module ActiveRecordApi
  module Rest
    class RequestUrlGenerator
      include ActiveAttr::Model
      attribute :request
      attribute :action_name
      attribute :parameters
      attribute :total_count
      delegate :host_with_port, :path, :query_parameters, to: :request
      delegate :limit, :offset, :page, to: :parameters

      def next_url
        return if total_count == 0 || total_count <= (page + 1) * limit
        "#{current_url}?#{new_params.to_param}"
      end

      def new_params
        query_parameters.dup.merge('page' => page + 1)
      end

      def current_url
        "#{protocol}#{host_with_port}#{micro_service_prefix}#{path}"
      end

      def micro_service_prefix
        request.headers['HTTP_X_REAL_PATH'].sub(path, '') if request.headers['HTTP_X_REAL_PATH'].present?
      end

      def protocol
        Rails.env.development? ? 'http://' : 'https://'
      end
    end
  end
end
