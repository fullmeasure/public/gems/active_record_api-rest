shared_examples 'all::rest::actions' do
  let(:authorize_read) { mock_valid_auth('read') }
  let(:authorize_create) { mock_valid_auth('manage') }
  let(:authorize_update) { mock_valid_auth('manage') }
  let(:authorize_delete) { mock_valid_auth('manage') }

  let(:factory_symbol) { model_klass.to_s.underscore.to_sym }
  let(:model_array) { [model] + create_list(factory_symbol, 4) }
  let(:base_model_klass) do
    if model_klass.superclass == ApplicationRecord
      model_klass
    else
      model_klass.superclass
    end
  end
  let(:host) { ActiveRecordApi::Rest::RequestUrlGenerator.new(request: request).current_url }
  let(:create_new_attributes) { serializer.new(model).as_json.except(:id) }
  let(:update_new_attributes) { serializer.new(model).as_json }
  let(:update_invalid_attributes) { serializer.new(update_invalid_model).as_json.merge(id: model.id) }
  describe 'All rest actions' do
    let(:serializer) { ActiveModelSerializers::SerializableResource }
    let(:model_klass) { described_class.controller_name.classify.constantize }
    let(:model) { create factory_symbol }
    it_behaves_like 'get::show'
    it_behaves_like 'get::index'
    it_behaves_like 'put::update' do
      let(:new_attributes) { update_new_attributes }
      let(:invalid_attributes) { update_invalid_attributes }
    end
    it_behaves_like 'post::create' do
      let(:new_attributes) { create_new_attributes }
    end
    it_behaves_like 'delete::delete'
  end
end

shared_examples 'get::show' do
  describe 'GET show' do
    context 'when authorized' do
      before(:each) do
        authorize_read
      end
      context 'when valid' do
        before(:each) do
          get :show, params: { id: model.to_param }
        end
        it { expect(response.status).to eq 200 }
        it { expect(response.body).to be_json_eql(serializer.new(model).to_json) }
      end
      context 'when not found' do
        before(:each) do
          get :show, params: { id: -1 }
        end
        it { expect(response.status).to eq 404 }
        it { expect(JSON.parse(response.body)['base']).to include "Couldn't find #{base_model_klass} with 'id'=-1" }
      end
    end
    context 'when not authorized' do
      before(:each) do
        request.headers['session'] = {}
        get :show, params: { id: model.to_param }
      end
      it { expect(response.status).to eq 403 }
      it { expect(JSON.parse(response.body)['base']).to include 'Access denied on show' }
    end
    context 'when bad fullmeasure_session' do
      before(:each) do
        get :show, params: { id: model.to_param }
      end
      it { expect(response.status).to eq 401 }
      it { expect(JSON.parse(response.body)['base']).to include 'No user for fullmeasure_session on show' }
    end
  end
end

shared_examples 'get::index' do
  describe 'GET index' do
    context 'when authorized' do
      before(:each) do
        authorize_read
      end
      context 'when one result' do
        before(:each) do
          model
          get :index
        end
        it { expect(response.status).to eq 200 }
        it { expect(response.headers['x-total']).to eq 1 }
        it { expect(response.headers['x-link-next']).to be nil }
      end

      context 'when paged results' do
        before(:each) do
          model_array
          get :index, params: { limit: 1, offset: 1 }
        end
        it { expect(response.status).to eq 200 }
        it { expect(response.headers['x-total']).to eq model_array.length }
        it { expect(response.headers['x-link-next']).to eq "#{host}?limit=1&offset=1&page=2" }
        it { expect(response.body).to be_json_eql model_klass.offset(1).limit(1).map { |o| serializer.new(o) }.to_json }
      end
    end
    context 'when not authorized' do
      before(:each) do
        request.headers['session'] = {}
        get :index
      end
      it { expect(response.status).to eq 403 }
      it { expect(JSON.parse(response.body)['base']).to include 'Access denied on index' }
    end
  end
end

shared_examples 'put::update' do
  let(:new_attributes) { serializer.new(model).as_json }
  let(:invalid_attributes) { serializer.new(update_invalid_model).as_json.merge(id: model.id) }
  let(:relative_url) { Rails.application.routes.url_helpers.url_for(controller: base_model_klass.to_s.underscore.pluralize, action: 'show', id: model.id, only_path: true) }
  describe 'PUT update' do
    context 'when authorized' do
      before(:each) do
        authorize_update
      end
      context 'valid' do
        before(:each) do
          put :update, params: new_attributes
        end
        it { expect(response.status).to eq 200 }
      end
      context 'when invalid param values provided' do
        before(:each) do
          put :update, params: invalid_attributes
          expect(update_invalid_model.valid?).to be false
        end
        it { expect(response.status).to eq(422) }
        it { expect(response.body).to be_json_eql(update_invalid_model.errors.to_json) }
      end
      context 'when invalid param keys provided' do
        before(:each) do
          put :update, params: new_attributes.merge('foobars' => 'stuff')
        end
        it { expect(response.status).to eq(422) }
        it { expect(response.body).to be_json_eql({ base: 'Extra parameters are not allow: foobars' }.to_json) }
      end
    end
    context 'when not authorized' do
      before(:each) do
        request.headers['session'] = {}
        get :show, params: new_attributes
      end
      it { expect(response.status).to eq 403 }
      it { expect(JSON.parse(response.body)['base']).to include 'Access denied on show' }
    end
  end
end

shared_examples 'post::create' do
  let(:model_id) { model.id }
  let(:new_params) { serializer.new(model).as_json(except: [:id]) }
  describe 'POST create' do
    context 'when authorized' do
      before(:each) do
        authorize_create
      end
      context 'when invalid' do
        before(:each) do
          expect_any_instance_of(model_klass).to receive(:save).and_return(false)
          post :create, params: new_attributes
        end
        it { expect(response.status).to eq 422 }
      end
      context 'when valid' do
        it 'creates record with attributes' do
          model.delete
          expect {
            post :create, params: new_params
          }.to change(model_klass, :count).by(1)
          expect(response.status).to eq 200
          expect(JSON.parse(response.body).keys).to contain_exactly('id',
                                                                    'organization_id',
                                                                    'column1',
                                                                    'column2',
                                                                    'column3',
                                                                    'created_at',
                                                                    'updated_at')
          expect(model_klass.last.id).not_to eq model_id
        end
      end
    end
    context 'when not authorized' do
      before(:each) do
        request.headers['session'] = {}
        post :create, params: new_attributes
      end
      it { expect(response.status).to eq 403 }
      it { expect(JSON.parse(response.body)['base']).to include 'Access denied on create' }
    end
  end
end

shared_examples 'delete::delete' do
  describe 'DELETE delete' do
    context 'when authorized' do
      before(:each) do
        authorize_delete
      end
      it 'remove record' do
        model
        before_count = model_klass.count
        delete :destroy, params: { id: model.id }
        expect(model_klass.count).to eq(before_count - 1)
      end
    end
    context 'when not authorized' do
      before(:each) do
        request.headers['session'] = {}
        delete :destroy, params: { id: model.id }
      end
      it { expect(response.status).to eq 403 }
      it { expect(JSON.parse(response.body)['base']).to include 'Access denied on destroy' }
    end
  end
end
