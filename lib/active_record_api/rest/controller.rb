module ActiveRecordApi
  module Rest
    class Controller < ApplicationController
      include GracefulErrors
      delegate :previous_id, :limit, :offset, :page, :queryable_params, :modifiable_params, :not_allowed_params, :required_parameters, to: :parameters
      delegate :next_url, to: :request_url_generator
      before_action :initialize_model, only: :create
      before_action :validate_params

      def index
        response.headers['x-total'] = models_count
        response.headers['x-link-next'] = next_url unless next_url.nil?
        render json: models_full_results.limit(limit).offset(offset).order(id: :asc), each_serializer: serializer
      end

      def show
        render json: model, serializer: serializer
      end

      # :nocov:
      def new
        action = request.query_parameters[:action]
        render json: authorized_models.send(action, queryable_params.deep_symbolize_keys), serializer: serializer
      end
      # :nocov:

      def create
        if model.save
          render json: model, serializer: serializer
        else
          render json: model.errors, status: :unprocessable_entity
        end
      end

      def update
        if model.update(modifiable_params)
          render json: model, serializer: serializer
        else
          render json: model.errors, status: :unprocessable_entity
        end
      end

      def destroy
        if model.destroy
          render json: model
        else
          # :nocov:
          render json: model.errors, status: :unprocessable_entity
          # :nocov:
        end
      end

      protected

      def validate_params
        render json: { base: "Extra parameters are not allow: #{not_allowed_params.join(', ')}" }, status: :unprocessable_entity if not_allowed_params.present?
        render json: { base: "Missing parameters: #{required_parameters.join(', ')}" }, status: :unprocessable_entity if required_parameters.present?
      end

      def model
        @model ||= authorized_models.find(queryable_params[:id])
      end

      def models_full_results
        @models_full_results ||= authorized_models.where(queryable_params)
      end

      def models_count
        @total = models_full_results.count
      end

      def initialize_model
        @model = model_klass.new(modifiable_params)
      end

      def authorized_models
        model_klass.where('1 = 1')
      end

      def model_klass
        @model_klass ||= controller_name.classify.constantize
      end

      def serializer
        "#{model_klass.name}Serializer".safe_constantize
      end

      def parameters
        @parameters ||= Parameters.new(
          model_klass: model_klass,
          controller_name: controller_name,
          params: params,
          action_name: action_name
        )
      end

      def request_url_generator
        RequestUrlGenerator.new(
          action_name: action_name,
          request: request,
          parameters: parameters,
          total_count: models_count
        )
      end
    end
  end
end
