# frozen_string_literal: true

module ActiveRecordApi
  module Rest
    VERSION = '2.0.3'.freeze
  end
end
