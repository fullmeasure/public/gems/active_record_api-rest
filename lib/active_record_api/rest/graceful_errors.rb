module ActiveRecordApi
  module Rest
    module GracefulErrors
      extend ActiveSupport::Concern

      included do
        rescue_from ActiveRecord::RecordNotFound do |exception|
          render status: :not_found, json: { base: exception.message }
        end

        rescue_from ActiveRecordApi::Rest::Auth::BadSessionException do |exception|
          render status: :unauthorized, json: { base: "No user for fullmeasure_session on #{exception.action} #{exception.controller}", message: exception.message }
        end

        rescue_from ActiveRecordApi::Rest::Auth::AccessDeniedException do |exception|
          render status: :forbidden, json: { base: "Access denied on #{exception.action} #{exception.controller}", message: exception.message }
        end
      end
    end
  end
end
