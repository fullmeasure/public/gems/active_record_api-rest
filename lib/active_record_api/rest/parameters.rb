module ActiveRecordApi
  module Rest
    class Parameters
      include ActiveAttr::Model
      attr_accessor :model_klass, :params, :controller_name, :action_name

      def modifiable_params
        @modifiable ||= params.permit!.to_h.select! { |key, _value| valid_params(modifiable_names).include?(key.to_sym) }
      end

      def queryable_params
        @queryable ||= params.permit!.to_h.select! { |key, _value| valid_params(queryable_names).include?(key.to_sym) }
      end

      def valid_params(base_params)
        base_valid_params = base_params + ['organization_id'] + [:organization_id] + ['id'] + [:id]
        clean(base_valid_params)
      end

      def limit
        @limit ||= params[:limit]&.to_i || 50
      end

      def offset
        @offset ||= (params[:offset]&.to_i || 0) + (limit * (page - 1))
      end

      def page
        @page ||= params[:page]&.to_i || 1
      end

      def additional_valid_params
        []
      end

      def rest_valid_params
        [:limit, :offset, :page]
      end

      def not_allowed_params
        @not_allowed_params ||= clean_request_names - (valid_params(queryable_names) + Array.wrap(rest_valid_params) + Array.wrap(additional_valid_params))
      end

      def required_parameters
        []
      end

      protected

      def clean_request_names
        params.keys.map(&:to_sym) - [:controller, :action, controller_name.to_sym, controller_name.singularize.to_sym]
      end

      def modifiable_names
        @modifiable_model_columns ||= clean(queryable_names - %i[created_at updated_at])
      end

      def queryable_names
        @queryable_model_columns ||= clean(model_klass.column_names.flatten)
      end

      def clean(attributes_to_clean)
        attributes_to_clean.map { |column_name| column_name.to_s.gsub('encrypted_', '').to_sym }.uniq
      end
    end
  end
end
