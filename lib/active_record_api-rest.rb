# frozen_string_literal: true

require 'active_support'
require 'active_attr'

module ActiveRecordApi
  module Rest
    extend ActiveSupport::Autoload
    autoload :Controller
    autoload :GracefulErrors
    autoload :RequestUrlGenerator
    autoload :Parameters
    autoload :VERSION
    module Auth
      extend ActiveSupport::Autoload
      autoload :Policy
      autoload :AccessDeniedException
      autoload :BadRequestException
      autoload :BadSessionException
      autoload :Controller
    end
  end
end

