require 'rails_helper'

RSpec.describe ModelAuthsController, type: :controller do
  describe 'all supported actions' do
    let(:model_array) do
      [model] + 4.times.map do
        create(:model_auth)
      end
    end
    let(:update_invalid_model) { build :model_auth, column2: nil }
    include_examples 'all::rest::actions'
  end
end
