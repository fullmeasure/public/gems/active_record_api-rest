require 'rails_helper'

RSpec.describe ModelsController, type: :controller do
  describe 'all supported actions' do
    let(:serializer) { ActiveModelSerializers::SerializableResource }
    let(:model_array) do
      [model] + 4.times.map do
        create(:model)
      end
    end
    let(:model) do
      create :model
    end
    context 'index' do
      before(:each) do
        model_array
        get :index
      end
      it { expect(response.status).to eq 200 }
      it { expect(response.headers['x-total']).to eq 5 }
      it { expect(response.body).to be_json_eql model_array.to_json }
    end
    context 'get' do
      before(:each) do
        model
        get :show, params: { id: model.to_param }
      end
      it { expect(response.status).to eq 200 }
    end
    context 'new' do
      before(:each) do
        model
        get :show, params: { id: model.to_param }
      end
      it { expect(response.status).to eq 200 }
    end
    context 'put' do
      let(:new_attributes) { serializer.new(model).as_json }
      before(:each) do
        model
        get :update, params: new_attributes
      end
      it { expect(response.status).to eq 200 }
    end
    context 'post' do
      let(:model) { build :model }
      let(:new_attributes) { serializer.new(model).as_json.except(:id) }
      before(:each) do
        get :create, params: new_attributes
      end
      it { expect(response.status).to eq 200 }

      it {
        expect(JSON.parse(response.body).keys).to contain_exactly('id',
                                                                  'organization_id',
                                                                  'column1',
                                                                  'column2',
                                                                  'column3',
                                                                  'created_at',
                                                                  'updated_at')
      }
    end
    context 'destroy' do
      before(:each) do
        get :destroy, params: { id: model.to_param }
      end
      it { expect(response.status).to eq 200 }
    end
  end
end
