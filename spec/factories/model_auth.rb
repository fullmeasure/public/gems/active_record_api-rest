FactoryBot.define do
  factory :model_auth do
    sequence(:column2) { |n| "#{n}#{Faker::TvShows::StarTrek.location}" }
    sequence(:organization_id)
  end
end
