FactoryBot.define do
  factory :model do
    sequence(:id)
    sequence(:column2) { |n| "#{n}#{Faker::TvShows::StarTrek.location}" }
    sequence(:organization_id)
  end
end
