module PermissionName
  def permission_name(model_klass, action)
    "#{service_name}__#{model_klass.name.underscore}:#{action}"
  end

  def service_name
    File.basename(Rails.root.to_s)
  end
end

RSpec.configure do |config|
  config.include PermissionName
end
