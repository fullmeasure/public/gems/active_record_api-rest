module AuthToken
  def mock_valid_auth(auth)
    request.headers['session'] = { auth => 'true' }
  end
end

RSpec.configure do |config|
  config.include AuthToken, type: :model
  config.include AuthToken, type: :controller
end
