class ModelSerializer < ActiveModel::Serializer
  attributes :id, :column1, :column2, :column3, :created_at, :updated_at, :organization_id
end
