class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def session
    return nil if request.headers['Session-Id'].blank?
    { user_id: 1, permissions: request.headers['Permissions'], permission_attributes: {} }.with_indifferent_access
  end
end
