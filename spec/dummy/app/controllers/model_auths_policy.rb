class ModelAuthsPolicy < ActiveRecordApi::Rest::Auth::Policy
  def can_manage?
    fullmeasure_session['manage'].present?
  end

  def can_read?
    fullmeasure_session['read'].present?
  end
end
