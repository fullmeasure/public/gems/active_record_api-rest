class User
  def initialize(permissions)
    @permissions = permissions
  end

  def full_permissions
    @permissions
  end
end